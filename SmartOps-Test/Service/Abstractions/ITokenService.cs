﻿using System;
using SmartOps_Test.Model;

namespace SmartOps_Test.Service.Abstractions
{
    public interface ITokenService
    {
        string BuildToken(string key, User user);
        bool IsTokenValid(string key, string token);
    }
}
