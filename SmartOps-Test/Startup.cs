﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Microsoft.EntityFrameworkCore;
using SmartOps_Test.DataContext;
using SmartOps_Test.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using SmartOps_Test.Service.Abstractions;
using SmartOps_Test.Service;
using System.Collections.Generic;

namespace SmartOps_Test
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var mysqlVersion = new MySqlServerVersionSetting();
            Configuration.GetSection("MySqlServerVersion").Bind(mysqlVersion);

            var jwtSetting = new JwtSetting();
            Configuration.GetSection("JwtSetting").Bind(jwtSetting);

            services.Configure<JwtSetting>(Configuration.GetSection("JwtSetting"));
            services.Configure<MailSettings>(Configuration.GetSection("MailSettings"));
            services.Configure<FireBaseSettings>(Configuration.GetSection("FireBaseSettings"));
            services.Configure<FrontEndSettings>(Configuration.GetSection("FrontEndSettings"));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "SmartOps Test",
                    Description = "A simple ASP.NET Core Web API to handle customer management",
                    Contact = new OpenApiContact
                    {
                        Name = "Dominic Foli",
                        Email = "codemaestro@outlook.com",
                        Url = new Uri("https://www.linkedin.com/in/dominic-foli-8275b4136/"),
                    }
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                      new OpenApiSecurityScheme
                      {
                        Reference = new OpenApiReference
                          {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                          },
                          Scheme = "oauth2",
                          Name = "Bearer",
                          In = ParameterLocation.Header,

                        },
                        new List<string>()
                      }
                });
            });

            var serverVersion = new MySqlServerVersion(new Version(mysqlVersion.Major, mysqlVersion.Minor, mysqlVersion.Build));
            services
                .AddDbContext<AppDbContext>
                (option => option.UseMySql(Configuration.GetConnectionString("DatabaseConnectionString"), serverVersion));

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<AppDbContext>();


            services.AddTransient<ITokenService, TokenService>();
            services.AddTransient<IEmailService, MailService>();
            services.AddTransient<IFireBaseService, FireBaseService>();

            services.AddControllers();

            // AddAutoMapper - to load all automapper profiles
            services.AddAutoMapper(typeof(Startup));

            //IDENTITY CONFIG
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = true;
            });

            //JWT CONFIG
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero,
                    IssuerSigningKey = new
                    SymmetricSecurityKey
                    (Encoding.UTF8.GetBytes
                    (jwtSetting.Key))
                };
            });


            //CORS CONFIG
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AppDbContext context, UserManager<User> userManager, RoleManager<Role> roleManager)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

           // app.UseHttpsRedirection();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            context.Database.Migrate();
            CreateRoles(roleManager, userManager);

            app.UseCors("MyPolicy");

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void CreateRoles(RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            bool x = roleManager.RoleExistsAsync("Admin").Result;
            if (!x)
            {
                // first we create Admin rool    
                var adminRole = new Role();
                adminRole.Name = "Admin";
                _ = roleManager.CreateAsync(adminRole).Result;

                var customerRole = new Role();
                customerRole.Name = "Customer";
                _ = roleManager.CreateAsync(customerRole).Result;

                //Here we create a Admin super user who will maintain the website                   

                var user = new User
                {
                    FirstName = "Admin",
                    LastName = "User",
                    UserName = "adminuser",
                    Email = "admin@smartops.com",
                    IsAdmin = true
                };
                string userPWD = "Password1";


                var chkUser = userManager.CreateAsync(user, userPWD).Result;

                //Add default User to Role Admin    
                if (chkUser.Succeeded)
                {
                    userManager.AddToRoleAsync(user, adminRole.Name);
                }
            }

        }
    }
}
