﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using SmartOps_Test.DataContext;
using SmartOps_Test.Model;
using SmartOps_Test.Service.Abstractions;

namespace SmartOps_Test.Utility
{
    public class Helper
    {
        public Helper()
        {
        }

        public static string SaveCustomerImage(IHostingEnvironment env, string ImgStr, string ImgName)
        {
            var path = Path.Combine(env.ContentRootPath, "ImageStorage");

            //Check if directory exist
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path); //Create directory if it doesn't exist
            }

            string imageName = ImgName + ".jpg";

            //set the image path
            string imgPath = Path.Combine(path, imageName);

            byte[] imageBytes = Convert.FromBase64String(ImgStr);

            File.WriteAllBytes(imgPath, imageBytes);

            return imageName;
        }

        public static string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName) + "_" + Guid.NewGuid().ToString().Substring(5, 5) + Path.GetExtension(fileName);
            
        }

        public static async Task<int> UploadAndSaveContent(AppDbContext context, IFireBaseService _firebaseService, string image, string fileName)
        {
            var contentResponse = FileDocument.Create(image, fileName, "Test", FileDocumentType.GetDocumentType(MIMETYPE.IMAGE));
            var uploadedContent = await _firebaseService.UploadDocumentAsync(contentResponse);

            if (uploadedContent == null || string.IsNullOrEmpty(uploadedContent.Path))
            {
                throw new Exception("Unable to upload Image try again!");
            }
            var content = new Content
            {
                ContentUrl = uploadedContent.Path
            };

            await context.Contents.AddAsync(content);
            await context.SaveChangesAsync();
            return content.Id;
        }

        public static string GenerateRandomString()
        {
            var bytes = new byte[4];
            var rng = RandomNumberGenerator.Create();
            rng.GetBytes(bytes);
            uint random = BitConverter.ToUInt32(bytes, 0) % 100000000;
            return String.Format("{0:D8}", random);
        }
    }
}
