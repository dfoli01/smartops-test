﻿using System;
using AutoMapper;
using SmartOps_Test.Model;
using SmartOps_Test.Model.ViewModel;

namespace SmartOps_Test.Utility
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, CustomerViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Company, opt => opt.MapFrom(src => src.Company))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.HasPaid, opt => opt.MapFrom(src => src.HasPaid))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Content.ContentUrl))
                .ForMember(dest => dest.TtlExpiryTime, opt => opt.MapFrom(src => src.TtlExpiryTime))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.Summary, opt => opt.MapFrom(src => src.Summary))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber));

            CreateMap<Memo, MemoViewModel>()
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
              .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Content))
              .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.MemoCategory.Name));
        }
    }
}
