﻿using System;
namespace SmartOps_Test.Model.InputModel
{
    public class LoginInputModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; } = true;
    }
}
