﻿using System;
namespace SmartOps_Test.Model
{
    public class MemoCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
