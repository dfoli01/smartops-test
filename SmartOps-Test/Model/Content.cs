﻿using System;
namespace SmartOps_Test.Model
{
    public class Content : BaseEntity
    {
        public Content()
        {
        }

        public string ContentUrl { get; set; }
    }
}
