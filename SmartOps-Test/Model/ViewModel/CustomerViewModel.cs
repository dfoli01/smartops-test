﻿using System;
namespace SmartOps_Test.Model.ViewModel
{
    public class CustomerViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? TtlExpiryTime { get; set; }
        public bool HasPaid { get; set; }
        public string Position { get; set; }
        public string Summary { get; set; }
    }
}
