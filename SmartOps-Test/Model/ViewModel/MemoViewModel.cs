﻿using System;
namespace SmartOps_Test.Model.ViewModel
{
    public class MemoViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string Content { get; set; }
    }
}
