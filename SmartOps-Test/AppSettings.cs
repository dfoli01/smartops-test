﻿using System;
namespace SmartOps_Test
{
    public class AppSettings
    {
        public AppSettings()
        {
        }
    }

    public class MySqlServerVersionSetting {
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Build { get; set; }
    }

    public class JwtSetting
    {
        public string Key { get; set; }
    }

    public class MailSettings
    {
        public string Mail { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
    }

    public class FireBaseSettings
    {
        public string FireBaseBucket { get; set; }
        public string FireBaseSenderKey { get; set; }
        public string UploadDrive { get; set; }
        public string DriveName { get; set; }

    }

    public class FrontEndSettings
    {
        public string BaseUrl { get; set; }
    }
}
